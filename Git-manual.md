# GIT Manual for Malinette
Some Git setup and commands.

## SSH Keys
- Open a Terminal
- ssh-keygen -t rsa -C "...@..."
- cat ~/.ssh/id_rsa.pub
- Go to [SSH Keys](https://git.framasoft.org/profile/keys) : Sign in > Profile Settings > SSH Keys > add Key

## Git commands

### Clone
git clone git@framagit.org:resonance/malinette.git malinette

### Branch
git config --global push.default simple
git checkout -b dev
git push --set-upstream origin dev
git checkout dev

git checkout -b myfeature develop
 
git checkout -b 7.x-2.x
git push -u origin 7.x-1.x

git push origin 7.x-2.x
git checkout 7.x-2.x
git branch -v

## Merge
git checkout master
git merge dev

##Tag
git tag -a v1.4 -m "my version 1.4"
git push origin v1
git push origin --tags

## Sub directory
git archive -o ../subarchive.zip HEAD:abs/malinette-abs
git archive --remote=http://bittracker.org/someproject.git HEAD:<path/to/directory/or/file> <filename> | tar -x
git submodule add https://framagit.org/malinette/malinette-ide.git
git archive --remote=https://framagit.org/malinette/malinette-ide.git HEAD | tar -x

## Subtree
Submodules can't allow you to download repo vit gitlab web interface. You need subtree.

- Add a sub repo : git subtree add --prefix malinette-ide https://framagit.org/malinette/malinette-ide.git master --squash
- Update it : git subtree pull --prefix malinette-ide https://framagit.org/malinette/malinette-ide.git master --squash
- git add *
- git commit
- git push

## Empty Folders
Git seems not track empty folders in subtree. The easiest way is to add a empty file .gitkeep for instance into each empty folders.

[Source](https://bytefreaks.net/gnulinux/bash/how-to-add-automatically-all-empty-folders-in-git-repository)

## Executable permission with git
On Linux, executables files are not sharing when you download the repo. We need to set up the permission witg git update-index or fileMode.

### One file
- Stage the script.sh file : git add script.sh 
- Flag it as executable : git update-index --chmod=+x script.sh
- Commit the change git commit -m "Make script.sh executable."
- Push the commit git push

### All files
[Source](https://stackoverflow.com/questions/14267441/automatically-apply-git-update-index-chmod-x-to-executable-files)

- Set fileMode = true in your .git/config file (or by running git config core.filemode true as others have pointed out)
- Change the executable bit on the file's permissions and commit this change. ( chmod u+x $script as you pointed out). You only have to do this once.
- Push to the remote

**Explanation**
The next time you pull from there, git will set the committed executable bit on the file. I also had similar problems, this solved them.

fileMode = true tells git to track the only thing about permissions it can: the executable bit. This means that changes to the executable bit will be recognized by git as changes in the working tree and those changes will be stored in the repo with your next commit.

Once you committed the desired executable bit you can also reset your fileMode to false so next time git won't bother you with such changes when you don't want to commit them.
