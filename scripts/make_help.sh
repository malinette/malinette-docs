#! /bin/sh
# Write descriptions of abstractions into a html file

# File input
fileobjects="../../malinette-ide/preferences/malinette-objects-list.txt"
#fileobjects="../../malinette-ide/preferences/brutbox-objects-list.txt"

# File output
filehtml="../../objects/malinette-objects.html" 

# Write into files
cat header.html > $filehtml

IFS="
"

for line in $(cat "$fileobjects")
do	
	# Find category
	category=$(echo $line | cut -d ":" -f1)
	echo "<div class=\"bloc\"><h2>$category</h2><ul>" >> $filehtml

	# Avoid the master category
	if [ $category != "master" ]; then
	
		objects=$(echo $line | cut -d ":" -f2 | cut -d ";" -f1)
		IFS=" "
		for object in $objects
		do
			patch="../../malinette-ide/abs/malinette-abs/$object-help.pd"

			# Special case with [average] which is not in the Malinette
			if [ $object = "average" ]; then
				description="Average of last N values (maxlib)"
			else
				description=$(cat $patch | tr "\n" " " | sed -nr 's/.*#X text .* \\; -----* \\;(.*)/\1/p' | cut -d ";" -f1 | sed 's/\\//')
			fi		
			
			# Add to html
			echo "<li><strong>$object</strong> : $description</li>" >> $filehtml
		done

	fi

	echo "</div>" >> $filehtml

done

echo "</body></html>" >> $filehtml