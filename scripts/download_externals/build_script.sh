#!/bin/bash
# Build script file to launch Pure Data and Malinette
# No argument = local malinette + linux sizes
# Argument : linux, win, mac to releases folders
# Usage example : ./build_script linux

# -------- PD SETTINGS ------------ #
PD_MAC="Pd-0.48-0-i386.app"
#externals="bassemu~ comport creb cyclone ext13 Gem ggee hcs iemguts iemlib list-abs mapping markex maxlib moocow moonlib motex pduino pix_fiducialtrack pmpd puremapping purepd sigpack tof zexy"
#libs="Gem zexy creb iemlib iemlib2 iem_t3_lib"
externals="bassemu~ comport cyclone ext13 Gem ggee hcs iemguts iemlib list-abs maxlib moocow pix_fiducialtrack pmpd puremapping purepd sigpack tof zexy"
libs="Gem zexy iemlib iemlib2 iem_t3_lib "
abs="audio core in numbers out video seq"
extra="extra externals tclplugins"

# ------ Test arguments ------ #
if [ -z "$1" ]
then
	OS="linux"
	DEST="local"
else
	OS="$1"
	DEST="releases"
fi

# ------ OS specific variables ------ #
if [ "$OS" = "linux" ]; then
	pd="pd"
	pd_bin="$pd/bin/pd"
	hid="hid"
	script="start.sh"
	malinette="malinette"
elif [ "$OS" = "mac" ]; then
	pd="\"\$DIR\"/\"\$PD\""	# variables in the header
	pd_bin="$pd/bin/pd"
	hid="hid"
	script="start.sh"
	malinette="./malinette"
elif [ "$OS" = "win" ]; then
	pd=".." 	# header change directories
	pd_bin="pd.exe"
	hid="hidin"
	script="start.bat"
	malinette="../../malinette"
else
	echo "Error, try those arguments : mac, win or linux"
	exit 1
fi

p_externals="externals"

# Add hid lib, depends on platform
externals="$externals $hid"

# ------ Scripts variables ------ #
header="include/header_$OS.txt"
if [ "$DEST" = "local" ]; then
	output="../$script"
	pd="./pd/linux/pd"
	pd_bin="$pd/bin/pd"
	p_externals="../externals"
	extra="extra ../externals ../../../tclplugins"
elif [ "$DEST" = "releases" ]; then
	output="../../releases/$OS/$script"
fi

# ------- Build paths --------- #
paths_malinette="-path $malinette/abstractions"
for a in $abs
do
	paths_malinette="$paths_malinette -path $malinette/abstractions/$a"
done 

for l in $libs
do
	paths_libs="$paths_libs -lib $l"
done 

for e in $extra
do
	paths_extra="$paths_extra -path $pd/$e"
done

for ext in $externals
do
	paths_externals="$paths_externals -path $pd/$p_externals/$ext"
done 

# ------- Build script --------- #

cat $header > $output
echo -e '\n' >> $output

# Add variable to mac script
if [ "$OS" = "mac" ]
then
	echo -e "PD=\"$PD_MAC/Contents/Resources\"\n" >> $output
fi

options="-noprefs -nostdpath -helppath $pd/doc/5.reference -font-weight normal -open $malinette/MALINETTE.pd"

echo "$pd_bin $options $paths_extra $paths_malinette $paths_externals $paths_libs" >> $output

# Add somes REM for windows (?)
if [ "$OS" = "win" ]
then
	echo "REM taskkill /t /F /im pd.exe" >> $output
	echo "REM taskkill /t /F /im cmd.exe" >> $output
fi