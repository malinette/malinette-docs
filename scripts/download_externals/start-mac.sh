#!/bin/bash
# Malinette launch script (Mac OS X)
# PREVENT RESUME
rm -Rf ~/Library/Saved\ Application\ State/org.puredata*
defaults write org.puredata NSQuitAlwaysKeepsWindows -int 0
defaults write org.puredata NSQuitAlwaysKeepsWindows -bool false
defaults write org.puredata.pd.wish NSQuitAlwaysKeepsWindows -int 0
defaults write org.puredata.pd.wish NSQuitAlwaysKeepsWindows -bool false

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

PD="Pd-0.48-0-i386.app/Contents/Resources"

"$DIR"/"$PD"/bin/pd -noprefs -nostdpath -helppath "$DIR"/"$PD"/doc/5.reference -font-weight normal -open "$DIR"/malinette-ide/MALINETTE.pd -path "$DIR"/"$PD"/extra -path "$DIR"/externals -path "$DIR"/malinette-ide/tclplugins -path "$DIR"/externals/bassemu~ -path "$DIR"/externals/comport -path "$DIR"/externals/creb -path "$DIR"/externals/cyclone -path "$DIR"/externals/ext13 -path "$DIR"/externals/Gem -path "$DIR"/externals/ggee -path "$DIR"/externals/hcs -path "$DIR"/externals/iemguts -path "$DIR"/externals/iemlib -path "$DIR"/externals/list-abs -path "$DIR"/externals/mapping -path "$DIR"/externals/markex -path "$DIR"/externals/maxlib -path "$DIR"/externals/moocow -path "$DIR"/externals/moonlib -path "$DIR"/externals/motex -path "$DIR"/externals/pduino -path "$DIR"/externals/pix_fiducialtrack -path "$DIR"/externals/pmpd -path "$DIR"/externals/puremapping -path "$DIR"/externals/purepd -path "$DIR"/externals/sigpack -path "$DIR"/externals/tof -path "$DIR"/externals/zexy -path "$DIR"/externals/hid  -lib Gem -lib zexy -lib iemlib2 -lib iem_t3_lib
