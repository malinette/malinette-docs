#! /bin/sh
# Download Pure Data external packages (via Deken searchs)
# jerome abel for Pd-Malinette Project (09/2017)

# Fill free to uncomment call for functions find_urls, download_externals, unzip_externals

external_file="externals.txt"


# function create a folder
create_folder()
{
	if [ -d "$1" ]; then
		rm -rf "$1"
		mkdir "$1"
		echo "Dossier $1 re-created"	
	else
	  	mkdir "$1"
	  	echo "Dossier $1 created"
	fi
}

create_folder "externals"

# --------------------- FIND URLS ------------------------------ #
# Write the list of externals 
# Arguments : $1 (Linux or Windows or Darwin), $2 (64 or 32, depends on externals ?)
# ! liens sans espace
# ! trouve linux pour Darwin et Darwin pour Linux !!
find_urls()
{
	arch="$1"
	proc="$2"
	output="externals/$arch-$proc.txt"
	echo "" > $output

	while IFS=':' read -r f1 f2 f3 f4 f5; do
		# Get package name
		package_name=$(echo "$f1" | tr -d '[:space:]')

		# Get package version
		f2=$(echo "$f2" | tr -d '[:space:]')
		package_version="$f2"
	    if [ -z "$f2" ]; then
	    	if [ "$arch" = "Windows" ]; then
	    		package_version="$f3"
	    	elif  [ "$arch" = "Darwin" ]; then
	    		package_version="$f4"
	    	elif  [ "$arch" = "Linux" ]; then
	    		package_version="$f5"
	    	fi
	    fi

	    package_version=$(echo "$package_version" | tr -d '[:space:]')
	    
	    # Find Urls
	    if [ -n "$package_version" ]; then
	    	url=$(curl -s http://deken.puredata.info/search?name=$package_name | grep  "$package_version"|  grep "$arch" | grep "$proc" |  cut -f2)
			if [ -z "$url" ]; then
				# Abstraction package (no platform specific in the name)
		    	url=$(curl -s http://deken.puredata.info/search?name=$package_name | grep $package_version | cut -f2)
		    fi
		    url=$(echo "$url" | tr -d '[:space:]')
		    if [ -n "$url" ]; then
		    	echo "$url \n" >> "$output"
			fi
	    fi
	done < $external_file
}

# ---------------------------------------------------- #

# find_urls "Windows" "i386-32"
# find_urls "Darwin" "i386-32"
 find_urls "Linux" "amd64-64"


# --------------------- DOWNLOAD ------------------------------ #


download_externals()
{

	url_file="$1"
	zip_dir="$2"
	
	# Dir destination
	create_folder "$zip_dir"
	
	# Loop download
	while IFS='\n' read -r url; do
		wget "$url" --directory-prefix="$zip_dir"
	done < $url_file
}

# ---------------------------------------------------- #

#download_externals "externals/Linux-amd64-64.txt" "externals/Linux"
#download_externals "externals/Windows-i386-32.txt" "externals/Windows"
#download_externals "externals/Darwin-i386-32.txt" "externals/Darwin"

# --------------------- UNZIP ------------------------------ #
unzip_externals()
{
	zip_dir="$1"

	# Dir destination
	unzip_dir="$2"
	create_folder "$unzip_dir"

	# Unzip loops ()
	for file in $(ls $zip_dir/*.zip) ; do
		unzip "$file" -d "$unzip_dir"
	done

	.tgz

	for file in $(ls $zip_dir/*.tar.gz) ; do
	  tar zxvf "$file" -C "$unzip_dir"
	done

	for file in $(ls $zip_dir/*.tgz) ; do
	  tar zxvf "$file" -C "$unzip_dir"
	done
}

# ---------------------------------------------------- #


#unzip_externals "externals/Linux" "externals/Linux/unzip"
#unzip_externals "externals/Windows" "externals/Windows/unzip"
#unzip_externals "externals/Darwin" "externals/Darwin/unzip"
 

# ---------------------------------------------------- #
# TESTS - NOT WORKING
# ---------------------------------------------------- #
libs="bassemu~ comport creb cyclone ext13 Gem ggee hcs hid hidin iemguts iemlib markex maxlib moocow moonlib motex pix_fiducialtrack pmpd sigpack tof zexy"
abs="list-abs mapping pduino puremapping purepd"
platforms="Windows Darwin Linux-amd64"

# Find URLs of Deken packages for each platform (not working well ...)
find_urls_2()
{
	output="URLS.txt"
	echo "MALINETTE PACKAGES URLS \n" > $output

	for arch in $platforms
	do
		echo "\n\n$arch\n**********\n" >> $output
		for package in $libs
		do
			url=$(curl -s http://deken.puredata.info/search?name=$package | grep $package | grep $arch | cut -f2)
			echo "$url\n" >> $output
		done
	done
	
	for package in $abs
	do
		url=$(curl -s http://deken.puredata.info/search?name=$package | grep $package | cut -f2)
		echo "$url\n" >> $output
	done
}