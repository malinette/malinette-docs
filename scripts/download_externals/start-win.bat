REM Malinette launching script
REM -lib iemlib1 problem with cyclone/gate

@ECHO OFF

cd pd
cd bin

pd.exe -noprefs -nostdpath -helppath ../doc/5.reference -font-weight normal -open ../../malinette-ide/MALINETTE.pd  -path ../extra -path ../../externals -path ../../malinette-ide/abs/malinette-abs -path ../../externals/bassemu~ -path ../../externals/comport -path ../../externals/creb -path ../../externals/cyclone -path ../../externals/ext13 -path ../../externals/Gem -path ../../externals/ggee -path ../../externals/hcs -path ../../externals/iemguts -path ../../externals/iemlib -path ../../externals/list-abs -path ../../externals/pduino -path ../../externals/mapping -path ../../externals/maxlib -path ../../externals/moocow -path ../../externals/pix_fiducialtrack -path ../../externals/pmpd -path ../../externals/puremapping -path ../../externals/purepd -path ../../externals/sigpack -path ../../externals/tof -path ../../externals/zexy -path ../../externals/hidin  -lib Gem -lib zexy -lib creb -lib iemlib2 -lib iem_t3_lib

REM taskkill /t /F /im pd.exe
REM taskkill /t /F /im cmd.exe
